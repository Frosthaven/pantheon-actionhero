import { Initializer, api, config, log } from "actionhero";
import * as path from "path";
import { spawn, exec, execFile } from "child_process";

export class myInitializer extends Initializer {
    constructor() {
      super();
      this.name = "autohotkey";
      this.loadPriority = 1000;
    }
  
    async initialize() {
      let self = this;

      api.autohotkey = {
        runScript: async (scriptName, extraParams = null) => {
          //runs an autohotkey script using it's full filepath
          const filePath = path.normalize(
            path.join(__dirname, "..", "..", `scripts/${scriptName}.ahk`)
          );
          let parameters = [filePath]
      
          if (extraParams) {
            parameters.push.apply(parameters, extraParams)
          }
          const child = execFile(config.app.ahk_binary, parameters);
          let error = "";
          for await (const chunk of child.stderr) {
              error += chunk;
          }
          const exitCode = await new Promise( (resolve, reject) => {
              child.on('close', resolve);
          });
          if (exitCode) {
              throw new Error(`error running autohotkey script`);
          }
        }
      };
    }
  }