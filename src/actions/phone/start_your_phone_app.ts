import { Action, api } from "actionhero";

export class start_your_phone_app extends Action {
  constructor() {
    super();
    this.name = "phone:start_your_phone_app";
    this.description = "Launches Microsoft Your Phone app and connects to it.";
  }

  async run() {
    let error = null
    try {
      await api.autohotkey.runScript("phone_mirror_on");
    } catch(err) {
      error = err;
    }

    if (error) {
      return {
        error: error
      }
    }
  }
}