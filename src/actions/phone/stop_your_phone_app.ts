import { Action, api } from "actionhero";

export class stop_your_phone_app extends Action {
  constructor() {
    super();
    this.name = "phone:stop_your_phone_app";
    this.description = "Close Microsoft Your Phone app.";
  }

  async run() {
    let error = null
    try {
      await api.autohotkey.runScript("phone_mirror_off");
    } catch(err) {
      error = err;
    }

    if (error) {
      return {
        error: error
      }
    }
  }
}