module.exports = {
  apps : [{
    name: "pantheon-actionhero",
    script: "./dist/server.js",
    autorestart: true,
    error: "./logs/error.log",
    out: "./logs/out.log",
    watch: [
    	"dist"
    ],
    env: {
      NODE_ENV: "production", // production or development
      PORT: 8080,
    },
    env_dev: { // if launched with pm2 start ecosystem.config.js --env dev
      NODE_ENV: "development",
      PORT: 8080
    }
  }]
}