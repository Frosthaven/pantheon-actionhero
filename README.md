# pantheon-actionhero

_Home automation server for doing cool things, running on actionherojs_

## Requirements
1. Windows 10 OS
2. [Your Phone App](https://www.microsoft.com/en-us/p/your-phone/9nmpj99vjbwv?activetab=pivot:overviewtab) and compatible smart phone
3. [NodeJS](https://nodejs.org/en/)
4. [Autohotkey](https://www.autohotkey.com/)

## Recommended
1. [Tasker for Android](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm) for communicating with this server

-----

## SERVER:

### Initialize
```bash
npm install
```

### Configure
Update the contents of `./appconfig.json` to match your system

### Develop
```bash
npm run dev
```
_This creates a live server that automatically refreshes when you edit files_

### Deploy
```bash
npm run build
npm run start
```

You can also use the provided ecosystem file to run actionhero from PM2:

```bash
pm2 start ecosystem.config.js
```
_This ecosystem file is configured to automatically restart itself when you deploy new code changes_

-----

## PHONE:
coming soon

-----

## Further Reading
[actionherojs.com](https://www.actionherojs.com/)